from  multiprocessing import Lock, Process
import random
import sys
import time

def calcul_square(nombres):
    for i in nombres:
        print(i * i)


def calcul_cube(nombres):
    for i in nombres:
        print(i * i * i)


if __name__ == '__main__':
    nombres = [2, 3, 8, 9, 12]
    processes = []
    i = 1
    while i < 2:
        pro = Process(target=calcul_cube,args=(nombres,))
        processes.append(pro)
        procc = Process(target=calcul_square, args=(nombres,))
        processes.append(procc)
        pro.start()
        procc.start()
        i = i + 1

        for proc in processes:
            pro.join()
            procc.join()

