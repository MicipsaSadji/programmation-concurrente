# ----------------------exo 1--------------
def isocele(a, b, c):
    return testIsocele(a, b, c) or testIsocele(a, c, b) or testIsocele(c, b, a)


def testIsocele(x1, x2, x3):
    return (x1 == x2) and (x3 < 2 * x1)


# ----------------------exo 2---------------
import math


def aire_ordonne(a, b, c):
    # u1 = min(a, b, c)
    # u3 = max(a, b, c)
    # u2 = max(u1, min(u3, a, b, c))
    [u1, u2, u3] = sorted([a, b, c])
    q = (math.pow(u1, 2) - math.pow(u2, 2) + math.pow(u3, 2)) / 2.0
    r = math.pow(u1, 2) * math.pow(u3, 2)
    # print(u1, u2, u3, r, q)
    return 0.5 * math.sqrt(r - math.pow(q, 2))


# ---------------------exo3----------------

def definit_triangle(a, b, c):
    s = (a + b + c) / 2
    return a > 0 and b > 0 and c > 0 and a < s and b < s and c < s


# -----------------------exo4----------------

def nb_triangles_speciaux(n, p):
    count = 0
    for a in range(n, p + 1):
        for b in range(a, p + 1):
            for c in range(b, p + 1):
                if definit_triangle(a, b, c) and is_speciaux(a, b, c):
                    count = count + 1
    return count


def is_speciaux(a, b, c):
    return aire_ordonne(a, b, c) == (a + b + c)


if __name__ == '__main__':
    print("-----------exo1----------------")
    print(isocele(4, 2, 3))
    print(isocele(4, 3, 3))
    print(isocele(4, 4, 4))
    print(isocele(4, 2, 4))
    print(isocele(4, 2, 2))
    print("-----------exo2----------------")
    print(aire_ordonne(4, 2, 3))
    print(aire_ordonne(4, 3, 3))
    print(aire_ordonne(4, 4, 4))
    print(aire_ordonne(3, 4, 5))
    print(aire_ordonne(13, 14, 15))
    print(aire_ordonne(1, 1, 1))
    print("-----------exo3----------------")
    print(definit_triangle(1, 1, 20))
    print(definit_triangle(4, 2, 3))
    print(definit_triangle(4, 4, 4))
    print(definit_triangle(5, 2, 4))
    print(definit_triangle(4, 6, 4))
    print("-----------exo4----------------")
    print(nb_triangles_speciaux(1, 20))
    print(nb_triangles_speciaux(1, 70))
    print(nb_triangles_speciaux(1, 100))
    print(nb_triangles_speciaux(1, 200))
    print(nb_triangles_speciaux(1, 500))
