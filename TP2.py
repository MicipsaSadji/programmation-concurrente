from collections import deque
def fibu(a):
    if a <= 2:
        return 1

    return fibu(a - 2) + fibu(a - 1)


def boucle():
    words = ['cat', 'window', 'defenestrate']
    for w in words:
        print(w, len(w))
    return w


def boucle_copie():
    words = ['cat', 'window', 'defenestrate']
    for w in words[:]:  # Loop over a slice copy of the entire list.
        if len(w) > 6:
            words.insert(0, w)
    return words


def boucle_range():
    for i in range(5):
        print(i)


def f(a, L=[]):
    L.append(a)
    return L


def prime_number():
    for n in range(2, 10):
        print(n)
        for x in range(2, n):
            print(n, x)
            if n % x == 0:
                print(n, 'equals', x, '*', n // x)
                break
        else:
            # loop fell through without finding a factor
            print(n, 'is a prime number')


def continu():
    for num in range(2, 10):
        if num % 2 == 0:
            print("Found an even number", num)
            continue
    print("Found a number", num)


def ask_ok(prompt, retries=4, reminder='Please try again!'):
    while True:
        ok = input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries < 0:
            raise ValueError('invalid user response')
        print(reminder)


# Aucun argument ne peut recevoir une valeur plus d’une fois
# def function(a):
#   pass
# function(0, a=0)

# utilisation de la fonction lambda
def make_incrementor(n):
    return lambda x: x + n


def foo(ham: str, eggs: str = 'eggs') -> str:
    print("Annotations:", foo.__annotations__)
    print("Arguments:", ham, eggs)
    return ham + ' and ' + eggs

#--------------------------------lecture et ecriture de fichier
def fichier():
    f = open('a.txt', 'r+')
    with open('a.txt') as f:
        read_data: str = f.readline()
        print(f.read())
    f.closed
#------------------------------class
def scope_test():
    def do_local():
        spam = "local spam"

    def do_nonlocal():
        nonlocal spam
        spam = "nonlocal spam"

    def do_global():
        global spam
        spam = "global spam"

    spam = "test spam"
    do_local()
    print("After local assignment:", spam)
    do_nonlocal()
    print("After nonlocal assignment:", spam)
    do_global()
    print("After global assignment:", spam)

scope_test()
print("In global scope:", spam)


class Complex:
    def __init__(self, realpart, imagpart):
        self.r = realpart
        self.i = imagpart
class Dog:
    kind = 'canine'
    def __init__(self, name):
        self.name = name

class Mapping:
    def __init__(self, iterable):
        self.items_list = []
        self.__update(iterable)

    def update(self, iterable):
        for item in iterable:
            self.items_list.append(item)

    __update = update

class MappingSubclass(Mapping):

    def update(self, keys, values):
        for item in zip(keys, values):
            self.items_list.append(item)

if __name__ == '__main__':
    l = [3, 1, 5, 8]
    print()
    print(fibu(4))
    print(boucle())
    print(boucle_copie())
    boucle_range()
    print(f(1))
    print(f(4))
    prime_number()
    continu()
#    ask_ok("dit ok")
    f = make_incrementor(42)
    print(f(5))
    make_incrementor(42)
    l.sort()
    print(l)
    print(foo('spam'))
#--------------------------------------listes
    fruit = ['banane', 'pomme', 'peche', 'poire']
    print(fruit.count('banane'))
    print(fruit)
    fruit.remove('pomme')
    print('remove' , fruit)
    fruit.append('mandarine')
    print('append mandarine ' , fruit)
    fruit.sort()
    print('sort' , fruit)
    print(fruit.pop())
#-----pile lifo(last in firt out): derniere entrée(append(x)), premiere sortie(pop(x))
    stack = [3, 4, 5]
    stack.append(6)
    stack.append(7)
    print(stack)
    stack.pop()
    print(stack)
    stack.pop()
    print(stack)
    stack.pop()
    print(stack)
#-----file fifo(firt in first out): premiere entrée, premiere sortie en utilisant la class collection.deque


    queue = deque(["Eric", "John", "Michael"])
    print(queue)
    queue.append("Terry")           # Terry arrives
    queue.append("Graham")          # Graham arrives
    print(queue)
    queue.popleft()                 # The first to arrive now leaves
    print(queue)
    queue.popleft()                 # The second to arrive now leaves
    print(queue)                    # Remaining queue in order of arrival

#------les ensemble------------------

    basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
    print(basket)  # show that duplicates have been removed



# Demonstrate set operations on unique letters from two words

    a = set('abracadabra')
    b = set('alacazam')
    print(a)  # unique letters in a
    print(a - b)  # letters in a but not in b
    print(a | b)  # letters in either a or b
    print(a & b)  # letters in both a and b
    print(a ^ b)  # letters in a or b but not both


#------les dictionnaire(clé:valeur)------------------
    print("----------------------------les dictionnaire---------------------")
    tel = {'jack': 4098, 'sape': 4139}
    tel['guido'] = 4127
    print(tel)
    print(tel['jack'])
    del tel['sape']
    tel['irv'] = 4127
    print(list(tel.keys()))
    print(sorted(tel.keys()))
    scope_test()

#--------class-------------------------
    x = Complex(3.0, -4.5)
    print(x.r, x.i)
    d = Dog('Fido')
    e = Dog("Buddy")
    print(d.kind)
    print(d.name)
    print(e.name)