import time
def compte_mots(s):
    words = s.split(" ")
    compte = 0
    for word in words:
        if word != "":
            compte += 1
    print(compte)


def remplace_multiple(s1, s2, n):
    carcters1 = list(s1)
    carcters2 = list(s2)
    length1 = len(carcters1)
    length2 = len(carcters2)
    pas = int(length1 / n)
    i = 0
    print(length1, length2)
    for i in range(1, pas):
        print(i, i * n, i - 1)
        carcters1[i * n] = carcters2[i - 1]
    print(carcters1)
    if length2 > i:
        carcters1.extend(carcters2[i:length2])
    return "".join((carcters1))


def termeU(n):
    if n >= 1:
        return termeU(n - 1) * pow(2, n) + n
    return 1


def serie(p):
    s = 0
    for n in range(0, p + 1):
        s += termeU(n)
    return s


def serie_v2(p):
    # u0
    un_1 = 1
    s = un_1
    for n in range(1, p + 1):
        un = un_1 * pow(2, n) + n
        un_1 = un
        s += un
    return s


def fac_recursive(n):
    if n == 0:
        return 1
    return n * fac_recursive(n-1)


def fac_iterative(n):
    p = 1
    for i in range(2,n+1):
        p *= i
    return p


if __name__ == '__main__':
    print("----------------------exo1------------------------")
    compte_mots("")
    compte_mots('il ingurgite impunément un iguane.')
    compte_mots('coursdeprogrammation')
    compte_mots(' Attention aux espaces consécutifs ou terminaux ')
    print("----------------------exo2------------------------")
    print(remplace_multiple("", "", 2))
    print(remplace_multiple("abacus", "oiseau", 2))
    print(remplace_multiple("hirondelles", "nid", 3))
    print("----------------------exo3-ennoncé1------------------------")
    print(termeU(0))
    print(termeU(1))
    print(termeU(5))
    print(termeU(10))
    print("----------------------exo3-ennoncé2------------------------")
    print(serie(0))
    print(serie(1))
    print(serie(5))
    print(serie(10))
    print("----------------------exo3-ennoncé3------------------------")
    print(serie_v2(0))
    print(serie_v2(1))
    print(serie_v2(5))
    print(serie_v2(10))
    print("----------------------exo3-ennoncé5------------------------")
    depart = time.clock()
    serie(100)
    arriver = time.clock()
    print("temps passe en second serie(100) ", arriver-depart)

    depart = time.clock()
    serie_v2(100)
    arriver = time.clock()
    print("temps passe en second avec serie_v2(100)", arriver-depart)

    depart = time.clock()
    serie(200)
    arriver = time.clock()
    print("temps passe en second avec serie(200)", arriver-depart)

    depart = time.clock()
    serie_v2(200)
    arriver = time.clock()
    print("temps passe en second serie_v2(200)", arriver-depart)


    depart = time.clock()
    serie(300)
    arriver = time.clock()
    print("temps passe en second avec serie(300)", arriver-depart)

    depart = time.clock()
    serie_v2(300)
    arriver = time.clock()
    print("temps passe en second avec serie_v2(300)", arriver-depart)



    depart = time.clock()
    serie_v2(1000)
    arriver = time.clock()
    print("temps passe en second avec serie_v2(1000)", arriver-depart)

    print("----------------------exo4------------------------")
    print(fac_recursive(1))
    print(fac_recursive(2))
    print(fac_recursive(3))
    print(fac_recursive(4))

    print(fac_iterative(1))
    print(fac_iterative(2))
    print(fac_iterative(3))
    print(fac_iterative(4))
