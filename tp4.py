from threading import Thread
import time


class Calcul(Thread):
    def __init__(self, nombres, type):
        Thread.__init__(self)
        self.nombres = nombres
        self.type = type
        self.type = type

    def run(self):
        print(self.type)
        if self.type == "square":
            self.calcul_square(self.nombres)
        if self.type == "cube":
            self.calcul_cube(self.nombres)

    def calcul_square(self, nombres):
        for i in nombres:
            print(self.type, i * i)

    def calcul_cube(self, nombres):
        for i in nombres:
            print(self.type, i * i * i)


def calcul_square(nombres):
    for i in nombres:
        print(i * i)


def calcul_cube(nombres):
    for i in nombres:
        print(i * i * i)


if __name__ == '__main__':
    nombres = [2, 3, 8, 9, 12]

    a = time.clock()
    # Création des threads
    thread_1 = Calcul(nombres, "cube")
    thread_2 = Calcul(nombres, "square")

    # Lancement des threads
    thread_1.start()
    thread_2.start()

    # Attend que les threads se terminent
    thread_1.join()
    thread_2.join()
    f1 = time.clock() - a

    b = time.clock()
    calcul_cube(nombres)
    calcul_square(nombres)
    f2 = time.clock() - b
    print("sans les threads", f1, "avec les threads", f2)
